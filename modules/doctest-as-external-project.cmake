include(ExternalProject)
find_package(Wget REQUIRED)

ExternalProject_Add(
  doctest-project
  PREFIX
  ${CMAKE_BINARY_DIR}/doctest
  DOWNLOAD_DIR
  doctest
  DOWNLOAD_COMMAND
  ${WGET_EXECUTABLE}
  https://raw.githubusercontent.com/onqtam/doctest/master/doctest/doctest.h
  TIMEOUT
  10
  UPDATE_COMMAND
  ""
  CONFIGURE_COMMAND
  ""
  BUILD_COMMAND
  ""
  INSTALL_COMMAND
  ""
  LOG_DOWNLOAD
  ON
)

ExternalProject_Get_Property(doctest-project download_dir)

add_library(doctest INTERFACE)
target_include_directories(doctest INTERFACE ${download_dir}/..)
target_compile_features(doctest INTERFACE cxx_std_11)
add_dependencies(doctest doctest-project)
add_library(doctest::doctest ALIAS doctest)