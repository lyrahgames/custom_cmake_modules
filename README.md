# Custom CMake Modules

## Usage

In the root directory of an existing git-project do the following.
    
    git submodule add https://gitlab.com/lyrahgames/custom_cmake_modules.git cmake

In the file `CMakeLists.txt` of the root directory add the following line.

    add_subdirectory(cmake)

### Example

Add Catch2 as an external project and link it to an existing target `unit_test`.

    include(catch2-as-external-project)
    target_link_libraries(unit_test PRIVATE catch2::catch)

The standard include `#include <catch2/catch.hpp>` can be used in the C++ source files.